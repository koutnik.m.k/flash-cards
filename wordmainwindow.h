#ifndef WORDMAINWINDOW_H
#define WORDMAINWINDOW_H

#include <QMainWindow>

#include "word.h"

class QPushButton;
class QLabel;
class QToolBar;
class QMenu;
class QCloseEvent;
class QAction;
class QStatusBar;
class Word;
class QWindow;
class QCheckBox;


class WordMainWindow : public QMainWindow
{
private:
    Q_OBJECT

    unsigned int w, h;
    Word *wordObject;
    bool translateSetup;
    bool definitionSetup;
    QString workDir;

    QWidget *wordWindow;
    QWidget *prevWindow;

    QMainWindow *settingWindow;
    QWidget *settingWidget;

    QAction *actmoveForward;
    QAction *actmoveBackward;
    QAction *actknownWord;
    QAction *actunknownWord;
    QAction *actswitchCardExpresion;
    QAction *acthelpAbout;

    QLabel *lblUploadFile;

    QPushButton *btnmoveForward;
    QPushButton *btnmoveBackward;
    QPushButton *btnknownWord;
    QPushButton *btnunknoenWord;
    QPushButton *btnswitchCardExpresion;
    QPushButton *btnQuit;
    QPushButton *btnloadFile;
    QPushButton *btnSettings;
    QPushButton *btnSave;

    QMenu *mnuWords;
    QMenu *mnuOption;
    QToolBar *toolBar;
    QStatusBar *statusBar;
    QAction *actuploadFile;
    QAction *actuploadKnown;
    QAction *actuploadUnKnown;
    QAction *actShowKnown;
    QAction *actShowUnKnown;
    QAction *actSettings;

    QCheckBox *offonToolTip;
    QCheckBox *setupTrans;
    QCheckBox *setupDef;

    //void closeEvent(QCloseEvent *);
    void setupActions();
    void settings();
    void setupLayout();

public slots:
    void setupNextWord();
    void setupPreviousWord();
    void loadFile();
    void changeToExpreDef();
    void knowFile();
    void unknownFile();
    void openSettings();
    void saveSettings();
    void uploadknown();
    void uploadUnknown();
    void uploadFile(QString filename);
    void helpAbout();
public:
    WordMainWindow(QWidget *parent = nullptr);
    ~WordMainWindow();
};

#endif // MAINWINDOW_H
