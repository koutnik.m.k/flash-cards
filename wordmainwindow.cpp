#include "wordmainwindow.h"
#include "word.h"
#include <QLabel>
#include <QStatusBar>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QMenuBar>
#include <QDebug>
#include <QAction>
#include <QMenu>
#include <QToolBar>
#include <QSettings>
#include <QMessageBox>
#include <QCloseEvent>
#include <QFont>
#include <QFileDialog>
#include <QWindow>
#include <QCheckBox>



WordMainWindow::WordMainWindow(QWidget *parent)
    :QMainWindow(parent), w(1680), h(1050)
{
    wordObject = new Word;
    resize(w, h);
    setupLayout();
    setupActions();
    settings();

    //loadSettings();
}

WordMainWindow::~WordMainWindow(){}


void WordMainWindow::setupLayout(){
    wordWindow = new QWidget;
    prevWindow = new QWidget;

    settingWindow = new QMainWindow;

    QFont font;
    font.setPointSize(50);
    QFont font2;
    font2.setPointSize(20);

    QVBoxLayout *lMain = new QVBoxLayout;
    lMain->setObjectName("MainLayout");

    QHBoxLayout *lPrev = new QHBoxLayout;
    lPrev->setObjectName("prevLayout2");

    QHBoxLayout *lButtons = new QHBoxLayout;
    btnQuit = new QPushButton(tr("&Quit"));

    actmoveBackward = new QAction(tr("&Previous word"));
    actmoveBackward->setToolTip(tr("it moves you to previous word", "tool tip for previous word action"));
    actmoveBackward->setStatusTip(tr("it moves you to previous word", "status tip for previous word action"));
    btnmoveBackward = new QPushButton(tr("&<"));
    //btnmoveBackward->setToolTip(tr("it moves you to previous word", "tool tip for next previous button"));
    btnmoveBackward->setStatusTip(tr("it moves you to previous word", "status tip for next previous button"));
    btnmoveBackward->setFixedSize(200,100);
    btnmoveBackward->setFont(font);
    lButtons->addWidget(btnmoveBackward);
    lButtons->addStretch();

    actknownWord = new QAction(tr("&Known word"));
    actknownWord->setToolTip(tr("Puts your word to known words", "tool tip for know word action"));
    actknownWord->setStatusTip(tr("it moves you to previous word", "status tip for know word action"));
    btnknownWord = new QPushButton(tr("&Known word"));
    //btnknownWord->setToolTip(tr("it moves you to previous word", "tool tip for knoww button"));
    btnknownWord->setStatusTip(tr("it moves you to previous word", "status tip for know button"));
    btnknownWord->setFixedSize(100,75);
    lButtons->addWidget(btnknownWord);
    lButtons->addStretch();

    actswitchCardExpresion = new QAction(tr("&Switch expresion"));
    actswitchCardExpresion->setToolTip(tr("switch card expression", "tool tip for switch card expression"));
    actswitchCardExpresion->setStatusTip(tr("switch card expression", "status tip for switch card expression"));
    btnswitchCardExpresion = new QPushButton;
    //btnswitchCardExpresion->setToolTip(tr("switch card expression", "tool tip for switch card expression"));
    btnswitchCardExpresion->setStatusTip(tr("switch card expression", "status tip for switch card expression"));
    btnswitchCardExpresion->setFixedSize(400, 600);
    btnswitchCardExpresion->setFont(font2);
    lButtons->addWidget(btnswitchCardExpresion);
    lButtons->addStretch();


    actunknownWord = new QAction(tr("&Unknown word"));
    actunknownWord->setToolTip(tr("Puts your word to known words", "tool tip for unknow word action"));
    actunknownWord->setStatusTip(tr("it moves you to previous word", "status tip for unknow word action"));
    btnunknoenWord = new QPushButton(tr("&Unknown word"));
    //btnunknoenWord->setToolTip(tr("Puts your word to unknown words", "tool tip for unknow button"));
    btnunknoenWord->setStatusTip(tr("it moves you to uknown word", "status tip for unknow button"));
    btnunknoenWord->setFixedSize(100,75);
    lButtons->addWidget(btnunknoenWord);
    lButtons->addStretch();


    actmoveForward = new QAction(tr("&Next word"));
    actmoveForward->setToolTip(tr("it moves you to another word", "tool tip for next word action"));
    actmoveForward->setStatusTip(tr("it moves you to another word", "status tip for next word action"));
    btnmoveForward = new QPushButton(tr(">"));
    //btnmoveForward->setToolTip(tr("it moves you to another word", "tool tip for next word action"));
    btnmoveForward->setStatusTip(tr("it moves you to another word", "status tip for next word action"));
    btnmoveForward->setFixedSize(200,100);
    btnmoveForward->setFont(font);
    lButtons->addWidget(btnmoveForward);

    actuploadFile = new QAction(tr("&Upload file"));
    actuploadFile->setToolTip(tr("upload file with your word", "tool tip for upload file action"));
    actuploadFile->setStatusTip(tr("it moves you to another word", "status tip for upload file action"));
    btnloadFile = new QPushButton(tr("&Please upload file"));
    //btnloadFile->setToolTip(tr("upload file with your word", "tool tip for upload file action"));
    btnloadFile->setStatusTip(tr("it moves you to another word", "status tip for upload file action"));
    btnloadFile->setFixedSize(600, 300);
    btnloadFile->setFont(font2);
    lblUploadFile = new QLabel(tr("&Upload file"));
    lblUploadFile->setText("Please upload your file");
    lblUploadFile->setFont(font2);
    lblUploadFile->setBuddy(btnloadFile);

    acthelpAbout = new QAction(tr("About"));

    actShowKnown = new QAction(tr("&Show known words"));
    actShowKnown->setToolTip(tr("show file of known words", "tool tip for show known words"));
    actShowKnown->setStatusTip(tr("show file of known words", "status tip for show known words"));
    actShowUnKnown = new QAction(tr("&Show unknown words"));
    actShowUnKnown->setToolTip(tr("show file of unknown words", "tool tip for show unknown words"));
    actShowUnKnown->setStatusTip(tr("show file of unknown words", "status tip for show unknown words"));

    actSettings = new QAction(tr("&Settings"));
    //tooltip
    //statustip
    btnSettings = new QPushButton(tr("Settings"));
    //tooltip
    //statustip

    actuploadKnown = new QAction(tr("&Upload known words"));
    //tooltip
    //statustip
    actuploadUnKnown = new QAction(tr("&Upload unknown words"));
    //tooltip
    //statustip

    mnuOption = new QMenu(tr("&Options"));
    mnuOption->addAction(actuploadFile);
    mnuOption->addAction(actShowKnown);
    mnuOption->addAction(actShowUnKnown);
    mnuOption->addAction(actSettings);
    mnuOption->addAction(actuploadKnown);
    mnuOption->addAction(actuploadUnKnown);
    mnuOption->addAction(acthelpAbout);
    this->menuBar()->addMenu(mnuOption);

    mnuWords = new QMenu(tr("&Menu"));
    mnuWords->addAction(actmoveForward);
    mnuWords->addAction(actmoveBackward);
    mnuWords->addSeparator();
    mnuWords->addAction(actknownWord);
    mnuWords->addAction(actunknownWord);
    this->menuBar()->addMenu(mnuWords);

    toolBar = new QToolBar(tr("&Tool Bar"));
    toolBar->addWidget(btnQuit);
    toolBar->addSeparator();
    toolBar->addWidget(btnSettings);
    this->addToolBar(toolBar);

    statusBar = new QStatusBar();

    //lPrev->addWidget(lblUploadFile);
    lPrev->addWidget(btnloadFile);
    prevWindow->setLayout(lPrev);
    //prevWindow->setEnabled(false);
    prevWindow->activateWindow();
    setCentralWidget(prevWindow);


    lMain->addLayout(lButtons);
    wordWindow->setLayout(lMain);
}

void WordMainWindow::setupActions(){
    connect(btnQuit, SIGNAL(clicked()), this, SLOT(close()));
    connect(btnmoveForward, SIGNAL(clicked()), this, SLOT(setupNextWord()));
    connect(actmoveForward, SIGNAL(triggered()), this, SLOT(setupNextWord()));
    connect(btnmoveBackward, SIGNAL(clicked()), this, SLOT(setupPreviousWord()));
    connect(actmoveBackward, SIGNAL(triggered()), this, SLOT(setupPreviousWord()));
    connect(btnswitchCardExpresion, SIGNAL(clicked()), this, SLOT(changeToExpreDef()));
    connect(actswitchCardExpresion, SIGNAL(triggered()), this, SLOT(changeToExpreDef()));
    connect(btnknownWord, SIGNAL(clicked()), this, SLOT(knowFile()));
    connect(btnunknoenWord, SIGNAL(clicked()), this, SLOT(unknownFile()));
    connect(btnloadFile, SIGNAL(clicked()), this, SLOT(loadFile()));
    connect(actuploadFile, SIGNAL(triggered()), this, SLOT(loadFile()));
    connect(btnSettings, SIGNAL(clicked()), this, SLOT(openSettings()));
    connect(actSettings, SIGNAL(triggered()), this, SLOT(openSettings()));
    connect(actShowKnown, SIGNAL(triggered()), this, SLOT(uploadknown()));
    connect(actShowUnKnown, SIGNAL(triggered()), this, SLOT(uploadUnknown()));
    connect(acthelpAbout, SIGNAL(triggered()), this, SLOT(helpAbout()));
}

void WordMainWindow::helpAbout(){
    QMessageBox::information(this, tr("About"), tr("Developed by Martin Koutnik"));
}

void WordMainWindow::setupNextWord(){
    std::string word;
    word = wordObject->printWordName(wordObject->moveWordForward());
    btnswitchCardExpresion->setText(QString::fromStdString(word));
    wordObject->changed = true;
}

void WordMainWindow::setupPreviousWord(){
    std::string word;
    word = wordObject->printWordName(wordObject->moveWordBackward());
    btnswitchCardExpresion->setText(QString::fromStdString(word));
    wordObject->changed = true;
}

void WordMainWindow::loadFile(){
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open file"), workDir, tr("Images (*.txt);;" "ALLfiles (*.*)"));
    if(!fileName.isEmpty()){
        QFileInfo info(fileName);
        workDir = info.absoluteDir().absolutePath();
    }
    uploadFile(fileName);
}

void WordMainWindow::uploadFile(QString fileName){
    if(fileName.isNull()){
        QMessageBox::warning(this, tr("Warning"), tr("Failed to load your file"));
    }
    this->statusBar->showMessage(tr("File was opened"));
    wordObject->readFile(fileName.toStdString());

    wordWindow->activateWindow();
    setCentralWidget(wordWindow);
    setupNextWord();
}

void WordMainWindow::uploadknown(){
    if(wordObject->getknownCard().size() == 0){
        QMessageBox::warning(this, tr("Warning"), tr("You don't have any known word."));
    }else{
        wordObject->cleanVector(wordObject->getCard());
        wordObject->assignVector(wordObject->getknownCard());
        wordObject->assignItemCounter(1);
        setupNextWord();
    }
}

void WordMainWindow::uploadUnknown(){
    if(wordObject->getUnknownCard().size() == 0){
        QMessageBox::warning(this, tr("Warning"), tr("You don't have any unknown word."));
    }else{
        wordObject->cleanVector(wordObject->getCard());
        wordObject->assignVector(wordObject->getUnknownCard());
        wordObject->assignItemCounter(2);
        setupNextWord();
    }
}

void WordMainWindow::changeToExpreDef(){
    std::string trans, def, word;

    word = wordObject->printWordName(wordObject->getWordPosition());
    trans = wordObject->printTranslation(wordObject->getWordPosition());
    def = wordObject->printDefinition(wordObject->getWordPosition());

    if(wordObject->changed == true ){
        if(translateSetup == true && definitionSetup == true){
        btnswitchCardExpresion->setText(QString::fromStdString(trans + '\n' + def));
        }else{
            if(translateSetup){
            btnswitchCardExpresion->setText(QString::fromStdString(trans));
            }
            if(definitionSetup){
            btnswitchCardExpresion->setText(QString::fromStdString(def));
            }
        }
        wordObject->changed = false;
    }else{
        btnswitchCardExpresion->setText(QString::fromStdString(word));
        wordObject->changed = true;
    }

}

void WordMainWindow::knowFile(){
    wordObject->knownfile(wordObject->getWordPosition());
    setupNextWord();
}

void WordMainWindow::unknownFile(){
    wordObject->unknowfile(wordObject->getWordPosition());
    setupNextWord();
}

void WordMainWindow::openSettings(){
    settingWidget->setVisible(true);
}

void WordMainWindow::saveSettings(){
    if(setupDef->checkState()){
        definitionSetup = true;
    }else{
        definitionSetup = false;
    }
    if(setupTrans->checkState()){
        translateSetup = true;
    }else{
        translateSetup = false;
    }
    if(translateSetup == false && definitionSetup == false){
        QMessageBox::warning(this,tr("Warning") ,tr("Please choose one of translation or definition or both"));
    }else{
        settingWidget->setVisible(false);
    }
}

void WordMainWindow::settings(){
    settingWidget = new QWidget;

    //offonToolTip = new QCheckBox(tr("button advise"));
    setupDef = new QCheckBox(tr("Definition"));
    setupTrans = new QCheckBox(tr("Translation"));
    btnSave = new QPushButton(tr("Save"));

    translateSetup = true;
    definitionSetup = true;

    if(definitionSetup){
        setupDef->setCheckState(Qt::Checked);
    }
    if(translateSetup){
        setupTrans->setCheckState(Qt::Checked);
    }

    QVBoxLayout *lSettings = new QVBoxLayout(settingWidget);

    lSettings->addWidget(offonToolTip);
    lSettings->addWidget(setupDef);
    lSettings->addWidget(setupTrans);
    lSettings->addWidget(btnSave);

    connect(btnSave, SIGNAL(clicked()), this, SLOT(saveSettings()));
}
