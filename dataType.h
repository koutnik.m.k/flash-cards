#ifndef DATATYPE_H
#define DATATYPE_H

#include <iostream>
#include <string>
#include <tuple>

typedef std::string Therm;
typedef std::tuple<Therm, Therm, Therm, Therm> wordData;

#endif // DATATYPE_H
