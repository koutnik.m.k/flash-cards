#ifndef WORD_H
#define WORD_H

#include <vector>
#include <array>
#include <iostream>
#include <fstream>
#include "dataType.h"

#define WORDNAME_INDEX = 0;
#define DEFINITION_INDEX = 1;
#define TRANSLATION_INDEX = 2;
#define ITEMCOUNTER_INDEX = 3;

//typename std::vector<wordData> T;

class Word
{
private:
    Therm wordname;
    Therm definition;
    Therm translation;
    unsigned int itemCounter;
    unsigned int itemCounterKnown;
    unsigned int itemCounterUnKnown;
    std::vector<wordData> card;
    std::vector<wordData> unknownCard;
    std::vector<wordData> knownCard;
    wordData wordDataInstance;
public:
    Word();
    ~Word(){}

    const wordData &getWord() const;
    void setWord(const wordData &newWord);

    std::vector<wordData> getCard() {return card;}
    void setCard(const std::vector<wordData> &newCard);

    wordData setWordArr(Therm wn, Therm df, Therm tr);

    wordData getWordPosition() {return card[itemCounter];}

    std::vector<wordData> getUnknownCard(){return unknownCard;}
    std::vector<wordData> getknownCard(){return knownCard;}

    std::vector<wordData>readFile(std::string filename);

    wordData moveWordForward();
    wordData moveWordBackward();
    void showOnScreen(std::vector<wordData> card);
    void unknowfile(wordData word);
    void knownfile(wordData word);
    void printOneWord(wordData word);
    std::string printWordName(wordData word);
    std::string printDefinition(wordData word);
    std::string printTranslation(wordData word);

    void cleanVector(std::vector<wordData> card);
    void assignVector(std::vector<wordData> cardData);
    void assignItemCounter(int possibility);

    bool changed = false;
};

#endif // WORD_H
